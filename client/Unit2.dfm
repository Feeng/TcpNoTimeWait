object Form2: TForm2
  Left = 227
  Top = 219
  Width = 722
  Height = 377
  Caption = 'Socket Demo Client'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object lbl1: TLabel
    Left = 25
    Top = 282
    Width = 25
    Height = 13
    Caption = 'Host:'
  end
  object lbl2: TLabel
    Left = 10
    Top = 307
    Width = 40
    Height = 13
    Caption = 'Content:'
  end
  object lbl3: TLabel
    Left = 192
    Top = 282
    Width = 22
    Height = 13
    Caption = 'Port:'
  end
  object Memo1: TMemo
    Left = 8
    Top = 8
    Width = 689
    Height = 265
    ReadOnly = True
    TabOrder = 0
  end
  object EdtIP: TEdit
    Left = 57
    Top = 279
    Width = 121
    Height = 21
    TabOrder = 1
    Text = '127.0.0.1'
  end
  object EdtContent: TEdit
    Left = 57
    Top = 303
    Width = 561
    Height = 21
    TabOrder = 2
    Text = 'Socket Demo Content ...'
  end
  object btn1: TButton
    Left = 624
    Top = 302
    Width = 75
    Height = 22
    Caption = '&Send'
    TabOrder = 3
    OnClick = btn1Click
  end
  object EdtPort: TEdit
    Left = 222
    Top = 279
    Width = 67
    Height = 21
    TabOrder = 4
    Text = '65000'
  end
  object btn2: TButton
    Left = 295
    Top = 278
    Width = 75
    Height = 22
    Caption = '&Connect'
    TabOrder = 5
    OnClick = btn2Click
  end
  object btn3: TButton
    Left = 375
    Top = 278
    Width = 75
    Height = 22
    Caption = '&Disconnect'
    TabOrder = 6
    OnClick = btn3Click
  end
  object IdTCPClient1: TIdTCPClient
    MaxLineAction = maException
    ReadTimeout = 0
    OnDisconnected = IdTCPClient1Disconnected
    OnConnected = IdTCPClient1Connected
    Port = 0
    Left = 552
    Top = 152
  end
  object IdAntiFreeze1: TIdAntiFreeze
    Left = 432
    Top = 128
  end
end

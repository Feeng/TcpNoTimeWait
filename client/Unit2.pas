unit Unit2;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, IdAntiFreezeBase, IdAntiFreeze, IdBaseComponent,
  IdComponent, IdTCPConnection, IdTCPClient;

type
  TForm2 = class(TForm)
    Memo1: TMemo;
    lbl1: TLabel;
    EdtIP: TEdit;
    EdtContent: TEdit;
    btn1: TButton;
    lbl2: TLabel;
    lbl3: TLabel;
    EdtPort: TEdit;
    btn2: TButton;
    btn3: TButton;
    IdTCPClient1: TIdTCPClient;
    IdAntiFreeze1: TIdAntiFreeze;
    procedure btn2Click(Sender: TObject);
    procedure btn3Click(Sender: TObject);
    procedure btn1Click(Sender: TObject);
    procedure IdTCPClient1Connected(Sender: TObject);
    procedure IdTCPClient1Disconnected(Sender: TObject);
  private
    { Private declarations }
    procedure Log(Msg:String);
  public
    { Public declarations }
  end;

var
  Form2: TForm2;

implementation

{$R *.dfm}

procedure TForm2.btn2Click(Sender: TObject);
begin
  if IdTCPClient1.Connected then Exit;

  try
    IdTCPClient1.Host := EdtIP.Text;
    IdTCPClient1.Port := StrToIntDef(EdtPort.Text, 65000);
    IdTCPClient1.Connect();
  except on e:Exception do
    Log(Format('Try connect to server %s:%s err, info %s ...',
      [EdtIP.Text, EdtPort.Text, e.Message]));
  end;
end;

procedure TForm2.Log(Msg: String);
begin
  Memo1.Lines.Add(DateTimeToStr(Now) + ' ' + Msg);
end;

procedure TForm2.btn3Click(Sender: TObject);
begin
  if not IdTCPClient1.Connected then Exit;

  try
    IdTCPClient1.Disconnect();
  except on e:Exception do
    Log(Format('Try disconnect from server %s:%s err, info %s ...',
      [EdtIP.Text, EdtPort.Text, e.Message]));
  end;
end;

procedure TForm2.btn1Click(Sender: TObject);
begin
  if (not IdTCPClient1.Connected) or (EdtContent.Text = '') then Exit;

  IdTCPClient1.WriteLn(EdtContent.Text);
  Log(IdTCPClient1.ReadLn());
end;

procedure TForm2.IdTCPClient1Connected(Sender: TObject);
begin
  Log(Format('Connected to %s:%s ...', [EdtIP.Text, EdtPort.Text]));
end;

procedure TForm2.IdTCPClient1Disconnected(Sender: TObject);
begin
  Log(Format('Disconnected from %s:%s ...', [EdtIP.Text, EdtPort.Text]));
end;

end.

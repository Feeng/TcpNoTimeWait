object Form1: TForm1
  Left = 273
  Top = 311
  Width = 676
  Height = 369
  Caption = 'Socket Demo Server'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object lbl1: TLabel
    Left = 8
    Top = 304
    Width = 61
    Height = 13
    Caption = 'Service Port:'
  end
  object Memo1: TMemo
    Left = 8
    Top = 8
    Width = 641
    Height = 281
    ReadOnly = True
    TabOrder = 0
  end
  object btn1: TButton
    Left = 496
    Top = 296
    Width = 75
    Height = 25
    Caption = '&Start'
    TabOrder = 1
    OnClick = btn1Click
  end
  object btn2: TButton
    Left = 576
    Top = 296
    Width = 75
    Height = 25
    Caption = 'S&top'
    TabOrder = 2
    OnClick = btn2Click
  end
  object EdtPort: TEdit
    Left = 72
    Top = 301
    Width = 121
    Height = 21
    TabOrder = 3
    Text = '65000'
  end
  object IdTCPServer1: TIdTCPServer
    Bindings = <>
    CommandHandlers = <>
    DefaultPort = 65000
    Greeting.NumericCode = 0
    MaxConnectionReply.NumericCode = 0
    OnConnect = IdTCPServer1Connect
    OnExecute = IdTCPServer1Execute
    OnDisconnect = IdTCPServer1Disconnect
    ReplyExceptionCode = 0
    ReplyTexts = <>
    ReplyUnknownCommand.NumericCode = 0
    Left = 560
    Top = 104
  end
end
